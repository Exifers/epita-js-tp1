import {getState, setState} from "../store";
/*
*
* export a function that adds a new element to the store.
*
* Rules:
* - add must be able to take either a single element
* or an array of new elements
* - you must use the functions from "../store"
*
*/

const add = (input) => {
    if (input === undefined) {return;}
    if (Array.isArray(input)) {
        setState([...getState(), ...input]);
    }
    else {
        setState([...getState(), input]);
    }
};

export default add;
