import add from './add';
import get from './get';
import {setState} from "../store";

beforeEach(() => {
    setState([]);
});

test('one element', () => {
    const elem = {};
    const elem2 = [];
    add([elem,elem2]);
    expect(get(elem)).toBe(elem);
});

test('two elements', () => {
    const elem = {};
    const elem2 = [];
    add([elem, elem, elem2]);
    expect(get(elem)).toBe(elem);
});

test('element not found', () => {
    const elem = {};
    const elem2 = [];
    add([elem]);
    expect(get(elem2)).toBe(undefined);
});

test('no argument', () => {
    expect(get()).toBe(undefined);
});