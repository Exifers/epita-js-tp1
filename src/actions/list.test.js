import add from './add';
import list from './list';
import {getState, setState} from "../store";

beforeEach(() => {
    setState([]);
});

test('multiple elements', () => {
    add([1,2,3]);
    expect(list()).toBe(getState());
});

test('no elements', () => {
    expect(list()).toBe(getState());
});