import {getState, setState} from "../store";
/*
*
* export a function that updates a single element from the store.
*
* Rules:
* - you must use the functions from "../store"
* - the updated element must not share the same reference as the previous one.
*
*/

const update = (item, newItem) => {
    const state = getState();
    const index = state.indexOf(item);
    if (index > -1) {
        const newState = state.slice(0);
        newState[index] = newItem;
        setState(newState);
    }
};

export default update;
