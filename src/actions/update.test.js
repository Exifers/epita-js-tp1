import add from './add';
import update from './update';
import {getState, setState} from "../store";

beforeEach(() => {
    setState([]);
});

test('one element', () => {
    const elem = {};
    const elem2 = [];
    const elem3 = "";
    add([elem, elem2]);
    update(elem, elem3);
    expect(getState()).toEqual([elem3, elem2]);
});

test('element not in state', () => {
    const elem = {};
    const elem2 = [];
    const elem3 = "";
    add([elem2]);
    update(elem, elem3);
    expect(getState()).toEqual([elem2]);
});

test('no argument', () => {
    const elem = {};
    add([elem]);
    update();
    expect(getState()).toEqual([elem]);
});
