import {getState} from "../store";
/*
*
* export a function that gets a single element from the store.
*
* Rules:
* - you must use the functions from "../store"
*
*/

const get = (item) => {
    return getState().find(current => current === item);
};

export default get;
