import add from './add';
import {getState, setState} from "../store";

beforeEach(() => {
    setState([]);
});

test('array', () => {
    add([1,2]);
    expect(getState()).toEqual([1,2]);
});

test('single element', () => {
    add(1);
    expect(getState()).toEqual([1]);
});

test('single element multiple', () => {
    add(1);
    add(1);
    expect(getState()).toEqual([1,1]);
});

test('empty array', () => {
    add([]);
    expect(getState()).toEqual([]);
});

test('no arguments', () => {
    add();
    expect(getState()).toEqual([]);
});