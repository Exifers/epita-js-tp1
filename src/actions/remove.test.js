import add from './add';
import remove from './remove';
import {getState, setState} from "../store";

beforeEach(() => {
    setState([]);
});

test('one element', () => {
    const elem = {};
    const elem2 = [];
    add([elem, elem2]);
    remove(elem);
    expect(getState()).toEqual([elem2]);
});

test('element not in state', () => {
    const elem = {};
    const elem2 = [];
    const elem3 = "";
    add([elem, elem2]);
    remove(elem3);
    expect(getState()).toEqual([elem, elem2]);
});

test('no argument', () => {
    remove();
    expect(getState()).toEqual([]);
});