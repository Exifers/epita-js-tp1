import {getState, setState} from "../store";
/*
*
* export a function that removes a single element from the store.
*
* Rules:
* - you must use the functions from "../store"
*
*/

const remove = (item) => {
    const state = getState();
    const index = state.indexOf(item);
    if (index > -1) {
        const newState = state.slice(0);
        newState.splice(index, 1);
        setState(newState);
    }
};

export default remove;
